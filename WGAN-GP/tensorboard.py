from tensorboardX import SummaryWriter

writer = SummaryWriter('runs/exp1')

def add_loss(G_loss, D_loss, real_accuracy, fake_accuracy, iter_index):
    writer.add_scalar('G_loss', G_loss, iter_index)
    writer.add_scalar('D_loss', D_loss, iter_index)
    writer.add_scalar('real_images_accuracy', real_accuracy, iter_index)
    writer.add_scalar('fake_images_accuracy', fake_accuracy, iter_index)


def add_image(real_images, fake_images, iter_index):
    writer.add_image('real_images', real_images, iter_index)
    writer.add_image('fake_images', fake_images, iter_index)