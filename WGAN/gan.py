import os
import argparse

import torch
import torchvision
import torchvision.transforms as transforms

import network
import tensorboard

#########
# Args
#########
parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, default=64, help='size of the batches')
parser.add_argument('--z_dim', type=int, default=100, help='dimensionality of the latent space')
parser.add_argument('--gpu_id', type=int, default=0)
parser.add_argument('--n_epochs', type=int, default=50, help='number of epochs of training')
args = parser.parse_args()
torch.cuda.set_device(args.gpu_id)
torch.backends.cudnn.benchmark = True

#########
# Data
#########
data_path = '/media/twilightsnow/data/MNIST'
os.makedirs(data_path, exist_ok=True)
dataset = torchvision.datasets.MNIST(data_path, train=True, download=True, transform=transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
]))
dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True, drop_last=True)
#########
# Model
#########
# Initialize generator and discriminator
generator = network.Generator(noise_dim=args.z_dim)
discriminator = network.Discriminator()
# Optimizers
#optimizer_G = torch.optim.RMSprop(generator.parameters(), lr=0.00005)
#optimizer_D = torch.optim.RMSprop(discriminator.parameters(), lr=0.00005)
optimizer_G = torch.optim.Adam(generator.parameters(), lr=0.0002, betas=(0.5, 0.999))
optimizer_D = torch.optim.Adam(discriminator.parameters(), lr=0.0002, betas=(0.5, 0.999))
# Send to GPU
generator.cuda()
discriminator.cuda()

#########
# Train
#########
for epoch in range(args.n_epochs):
    for i, (imgs, _) in enumerate(dataloader):
        real_images = imgs.cuda()
        # -----------------
        #  Generate Images
        # -----------------
        # Sample noise as generator input
        z = (torch.rand(args.batch_size, args.z_dim) - 0.5) / 0.5  # -1 ~ 1 的分布
        z = z.cuda()

        # Generate a batch of images
        fake_images = generator(z)

        # -----------------
        #  Train Generator
        # -----------------
        # Loss measures generator's ability to fool the discriminator
        if i % 5 == 0:
            fake_images_pred = discriminator(fake_images)
            g_loss = -torch.mean(fake_images_pred)

            # Update network
            optimizer_G.zero_grad()
            g_loss.backward()
            optimizer_G.step()

        # ---------------------
        #  Train Discriminator
        # ---------------------
        # Measure discriminator's ability to classify real from generated samples
        fake_images_pred = discriminator(fake_images.detach())
        real_images_pred = discriminator(real_images)
        d_loss = -torch.mean(real_images_pred) + torch.mean(fake_images_pred)

        # Update network
        optimizer_D.zero_grad()
        d_loss.backward()
        optimizer_D.step()
        # Clip weights of discriminator
        for p in discriminator.parameters():
            p.data.clamp_(-0.01, 0.01)

        tensorboard.add_loss(g_loss.item(), d_loss.item(),
                             torch.mean(real_images_pred).item(), torch.mean(fake_images_pred).item(),
                             i + epoch * len(dataloader))
        if (i + 1) % 100 == 0:
            print('Epoch [{}/{}], Batch [{}/{}], G_Loss: {:.4f}, D_Loss: {:.4f}, Fake_Acc: {:.4f}, Real_Acc: {:.4f}'
                  .format(epoch, args.n_epochs, i, len(dataloader), g_loss.item(), d_loss.item(),
                          torch.mean(fake_images_pred).item(), torch.mean(real_images_pred).item()))
            tensorboard.add_image(torchvision.utils.make_grid(real_images, normalize=True),
                                  torchvision.utils.make_grid(fake_images, normalize=True), i + epoch * len(dataloader))
