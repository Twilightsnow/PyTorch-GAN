import torch
import torch.nn as nn

class Generator(nn.Module):
    def __init__(self, noise_size=100):
        super(Generator, self).__init__()
        self.noise_size = noise_size
        self.ngf = 64

        self.model = nn.Sequential(
            nn.ConvTranspose2d(self.noise_size, self.ngf * 8, kernel_size=4, stride=1),  # 4 * 4
            nn.BatchNorm2d(self.ngf * 8),
            nn.ReLU(),
            nn.ConvTranspose2d(self.ngf * 8, self.ngf * 4, kernel_size=4, stride=2, padding=1),  # 8 * 8
            nn.BatchNorm2d(self.ngf * 4),
            nn.ReLU(),
            nn.ConvTranspose2d(self.ngf * 4, self.ngf * 2, kernel_size=4, stride=2, padding=1),  # 16 * 16
            nn.BatchNorm2d(self.ngf * 2),
            nn.ReLU(),
            nn.ConvTranspose2d(self.ngf * 2, 3, kernel_size=4, stride=2, padding=1),
            nn.Tanh()
        )

    def forward(self, z):
        x = z.view(-1, self.noise_size, 1, 1)
        x = self.model(x)
        return x


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        self.ndf = 64
        self.model = nn.Sequential(
            nn.utils.spectral_norm(nn.Conv2d(3, self.ndf, kernel_size=4, stride=2, padding=1)),  # 16 * 16
            nn.LeakyReLU(0.2),
            nn.utils.spectral_norm(nn.Conv2d(self.ndf, self.ndf * 2, kernel_size=4, stride=2, padding=1)),  # 8*8
            nn.LeakyReLU(0.2),
            nn.utils.spectral_norm(nn.Conv2d(self.ndf * 2, self.ndf * 4, kernel_size=4, stride=2, padding=1)),  # 4*4
            nn.LeakyReLU(0.2),
        )
        self.fc = nn.Sequential(
            nn.utils.spectral_norm(nn.Linear(self.ndf * 4 * 16, 1)),
            nn.Sigmoid()
        )

    def forward(self, x):
        x = self.model(x)
        x = x.view(x.shape[0], -1)
        x = self.fc(x)
        return x


class GAN(object):
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.netG = Generator()
        self.netD = Discriminator()
        print(self.netG)
        print(self.netD)
        self.netG.cuda()
        self.netD.cuda()
        #self.criterion = nn.BCELoss()
        self.criterion = self.ls_loss
        self.real_label = torch.full((self.batch_size, 1), 1.0).cuda()
        self.fake_label = torch.full((self.batch_size, 1), 0.0).cuda()
        self.netG.apply(self.weights_init)
        self.netD.apply(self.weights_init)

    def weights_init(self, m):
        classname = m.__class__.__name__
        if classname.find('Conv') != -1:
            nn.init.xavier_uniform_(m.weight.data, 1.)
        elif classname.find('BatchNorm') != -1:
            m.weight.data.normal_(1.0, 0.02)
            m.bias.data.fill_(0)
        elif classname.find('Linear') != -1:
            nn.init.xavier_uniform_(m.weight.data, 1.)
            if m.bias is not None:
                m.bias.data.fill_(0.0)

    def ls_loss(self, pred, labels):
        return torch.mean((pred - labels) ** 2)

    def forward(self, real_images, z):
        self.fake_images = self.netG(z)
        self.real_images = real_images

    def backward_G(self):
        fake_images_pred = self.netD(self.fake_images)
        self.loss_g = self.criterion(fake_images_pred, self.real_label)
        self.loss_g.backward()

    def backward_D(self):
        self.pred_fake = self.netD(self.fake_images.detach())  # stop backprop to the generator by detaching fake_images
        fake_loss = self.criterion(self.pred_fake, self.fake_label)
        self.pred_real = self.netD(self.real_images)
        real_loss = self.criterion(self.pred_real, self.real_label)
        self.loss_d = (fake_loss + real_loss) / 2
        self.loss_d.backward()
