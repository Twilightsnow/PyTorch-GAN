from tensorboardX import SummaryWriter

class tensorboard(object):
    def __init__(self, dir):
        self.writer = SummaryWriter(dir)

    def add_loss(self, G_loss, D_loss, real_accuracy, fake_accuracy, iter_index):
        self.writer.add_scalar('G_loss', G_loss, iter_index)
        self.writer.add_scalar('D_loss', D_loss, iter_index)
        self.writer.add_scalar('real_images_accuracy', real_accuracy, iter_index)
        self.writer.add_scalar('fake_images_accuracy', fake_accuracy, iter_index)

    def add_image(self, real_images, fake_images, iter_index):
        self.writer.add_image('real_images', real_images, iter_index)
        self.writer.add_image('fake_images', fake_images, iter_index)