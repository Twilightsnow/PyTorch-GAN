import os
import argparse
import time

import torch
import torchvision
import torchvision.transforms as transforms

import gan
import tensorboard
import numpy as np

#########
# Args
#########
parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, default=64, help='size of the batches')
parser.add_argument('--z_dim', type=int, default=100, help='dimensionality of the latent space')
parser.add_argument('--gpu_id', type=int, default=1)
parser.add_argument('--n_epochs', type=int, default=100, help='number of epochs of training')
parser.add_argument('--manualSeed', type=int, help='manual seed', default=0)
parser.add_argument("--result_dir", type=str, default="runs/exp")
args = parser.parse_args()

torch.cuda.set_device(args.gpu_id)
np.random.seed(args.manualSeed)
torch.manual_seed(args.manualSeed)
torch.cuda.manual_seed(args.manualSeed)
torch.cuda.manual_seed_all(args.manualSeed)
# torch.backends.cudnn.benchmark = True
torch.backends.cudnn.deterministic = True

tensorboard = tensorboard.tensorboard(args.result_dir)
#########
# Data
#########
data_path = '/media/twilightsnow/data/CIFAR10'
os.makedirs(data_path, exist_ok=True)
dataset = torchvision.datasets.CIFAR10(data_path, train=True, download=True, transform=transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
]))
dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batch_size, shuffle=True, drop_last=True)

#########
# Model
#########
GAN = gan.GAN(args.batch_size)
optimizer_G = torch.optim.Adam(GAN.netG.parameters(), lr=0.0002, betas=(0.5, 0.999))
optimizer_D = torch.optim.Adam(GAN.netD.parameters(), lr=0.0002, betas=(0.5, 0.999))

#########
# Train
#########

for epoch in range(args.n_epochs):
    start_time = time.time()
    for i, (imgs, _) in enumerate(dataloader):

        # -----------------
        #  Generate Images
        # -----------------
        # Sample noise as generator input
        z = (torch.rand(args.batch_size, args.z_dim) - 0.5) / 0.5
        z = z.cuda()
        real_images = imgs.cuda()
        GAN.forward(real_images, z)

        # -----------------
        #  Train Generator
        # -----------------
        optimizer_G.zero_grad()
        GAN.backward_G()
        optimizer_G.step()

        # ---------------------
        #  Train Discriminator
        # ---------------------
        z = (torch.rand(args.batch_size, args.z_dim) - 0.5) / 0.5
        z = z.cuda()
        GAN.forward(real_images, z)
        optimizer_D.zero_grad()
        GAN.backward_D()
        optimizer_D.step()

        tensorboard.add_loss(GAN.loss_g.item(), GAN.loss_d.item(),
                             torch.mean(GAN.pred_fake).item(), torch.mean(GAN.pred_real).item(),
                             i + epoch * len(dataloader))
        if (i + 1) % 100 == 0:
            print('Epoch[{}/{}] Batch[{}/{}] G_Loss:{:.4f} D_Loss:{:.4f} Fake_Acc:{:.4f} Real_Acc:{:.4f}'
                  .format(epoch, args.n_epochs, i, len(dataloader), GAN.loss_g.item(), GAN.loss_d.item(),
                          torch.mean(GAN.pred_fake).item(), torch.mean(GAN.pred_real).item()))
            tensorboard.add_image(torchvision.utils.make_grid(GAN.real_images, normalize=True),
                                  torchvision.utils.make_grid(GAN.fake_images, normalize=True),
                                  i + epoch * len(dataloader))
    end_time = time.time()
    print('Epoch[{}/{}] Batch[{}/{}] G_Loss:{:.4f} D_Loss:{:.4f} Fake_Acc:{:.4f} Real_Acc:{:.4f} Time:{:.2f}s'
          .format(epoch, args.n_epochs, i, len(dataloader), GAN.loss_g.item(), GAN.loss_d.item(),
                  torch.mean(GAN.pred_fake).item(), torch.mean(GAN.pred_real).item(), end_time - start_time))
